1. Created MyCustomException, add it to signature and throw it in 
LifeCycleAction.execute() method.

2. Created SomeResource class and test it in main() method. Implemented
close() method just print close report into console.

3. Created ObjectThrowingException class and test it in main() method.
Variable has not been initialized because constructor throw exception
and assignment operation has not been complete.