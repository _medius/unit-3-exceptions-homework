package io.humb1t;

public class SomeResource implements AutoCloseable {

	@Override
	public void close() throws Exception {
		System.out.println("SomeResource has been closed.");
	}
}
