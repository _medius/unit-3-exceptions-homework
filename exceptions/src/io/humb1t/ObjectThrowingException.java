package io.humb1t;

public class ObjectThrowingException {

	public ObjectThrowingException() throws Exception {
		throw new Exception("Exception from constructor.");
	}

	@Override
	public String toString() {
		return "ObjectThrowingException{}";
	}
}
