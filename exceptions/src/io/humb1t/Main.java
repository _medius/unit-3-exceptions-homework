package io.humb1t;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        try {
            new LifeCycleAction().execute();
        } catch (LifeCycleActionExecutionException | AccessDeniedException | MyCustomExeption e) {
            System.err.println(e.getLocalizedMessage());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        try (FileInputStream fileInputStream = new FileInputStream(args[0])) {

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }


        try (SomeResource resource = new SomeResource()) {
        } catch (Exception e) {
            e.printStackTrace();
        }


        ObjectThrowingException objectThrowingException = null;
        try {
            objectThrowingException = new ObjectThrowingException();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(objectThrowingException);
    }

    public static class LifeCycleAction {
        public void execute() throws LifeCycleActionExecutionException, AccessDeniedException, MyCustomExeption {
            throw new MyCustomExeption("Exception cause.");
        }
    }

    public static class LifeCycleActionExecutionException extends Exception {
    }


    public static class MyCustomExeption extends Exception {

        public MyCustomExeption(String message) {
            super(message);
        }
    }

    public void exceptionVsResult() {
        final String result1 = (String) this.returnResult().value;
        final String result2 = returnOptional().orElse("");
        String result3 = "";
        try {
            result3 = returnValueOrThrowException();
        } catch (AccessDeniedException e) {
        }
    }

    private Result returnResult() {
        return Result.OK.setValue("OK");
    }

    private Optional<String> returnOptional() {
        return Optional.of("OK");
    }

    private String returnValueOrThrowException() throws AccessDeniedException {
        return "OK";
    }
}
